package Lesson7;


//Найти максимальное значение в целочисленном массиве из n-элементов
//(решить задачу с помощью for)

public class Task_2 {
    public static void main(String[] args) {
        int [] data = {28,25,45,12,146};
        int max = data[0];
        for (int i = 0; i < data.length ; i++) {
            if(data[i] > max)
                max = data[i];


        }
        System.out.println(max);
    }
}
