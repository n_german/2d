package Lesson7;


public class Task_5 {
    //В произвольной квадратной матрице найти минимальный элемент на главной
    //диагонали
    public static void main(String[] args) {
        int min = Integer.MAX_VALUE;

        int[][] array = {
                {90, 5, 7, 23},
                {7, 6, 4, 2},
                {8, 9, 33, 68},
                {0, 76, 22, 44}
        };
        for (int i = 0; i < array.length; i++) {
            if (min > array[i][i])
                min = array[i][i];
        }
        System.out.println(min);
    }
}
