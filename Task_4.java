package Lesson7;

public class Task_4 {
    public static void main(String[] args) {
        //В целочисленном массиве из n-элементов найти сумму элементов, следующих
        //после элемента, равного единице, вывести эту сумму; если такой элемент
        //отсутствует, вывести ноль
        int[] data = {3, 4, 55, 5, 1, 9, 8, 55};
        int sum = 0;
        boolean flag = false;
        for (int i = 0; i < data.length; i++) {
            if (flag)
                sum += data[i];
            else if (data[i] == 1)
                flag = true;

        }
        System.out.println(sum);


    }
}
